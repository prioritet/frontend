import { CountUp } from 'countup.js';

const counter = {
	onComplete(el) {
		el.setAttribute('style', 'width: auto;');
	},
	init() {
		const elements = document.querySelectorAll('[data-counter]');

		Array.prototype.forEach.call(elements, (number) => {
			// If has separator
			const separator = number.hasAttribute('data-counter-separator');

			let endVal = number.getAttribute('data-counter') || number.innerHTML;

			endVal = endVal.replace(',', '.').trim();

			const temp = endVal.split('.');
			let decimals = 0;

			if (temp.length === 2) {
				decimals = temp[1].length;
			}

			// Set width
			const width = number.offsetWidth;

			number.setAttribute('style', `width: ${width}px;`);

			if(separator) {
				endVal = endVal.replace(' ', '').trim();
			}

			const count = new CountUp(number, endVal, {
				startVal: 0,
				separator: separator ? ' ' : '',
				decimal: ',',
				decimalPlaces: decimals,
			});

			if (number.classList.contains('in-viewport')) {
				count.start(this.onComplete.bind(this, number));
			}

			number.addEventListener('sal:in', () => {
				count.start(this.onComplete.bind(this, number));
			});

			number.addEventListener('sal:out', () => {
				count.reset();
			});
		});
	},
};

export default counter;
