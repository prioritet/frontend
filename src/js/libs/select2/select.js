import PerfectScrollbar from 'perfect-scrollbar';

require('select2/dist/js/select2.full.js');
// require('./select2position');
require('./select2multi');

const select = {
	init() {
		this.initSelect();
		window.addEventListener('init.select', () => {
			this.initSelect();
		});
	},
	initSelect() {
		const elements = document.querySelectorAll('[data-select]');
		$.fn.select2.amd.require([
			'select2/multi-checkboxes/dropdown',
			'select2/multi-checkboxes/selection',
			'select2/multi-checkboxes/results',
		], (DropdownAdapter, SelectionAdapter, ResultsAdapter) => {
			elements.forEach((el) => {
				if (el.initialized) return;
				const $select = $(el);
				const isMultiple = !!$select.prop('multiple');
				const closeOnSelect = !isMultiple;
				const size = $select.data('select-size') || 'default';
				const placeholder = $select.data('select-placeholder') || $select.attr('placeholder') || false;
				const $form = $select.closest('form');
				const hasSearch = $select.data('select-search');
				const search = hasSearch ? 0 : Infinity;
				const searchPlaceholder = $select.data('search-placeholder') || 'Поиск';

				const texts = $select.data('texts') || false;

				// Icon class
				const icon = $select.data('select-icon');
				let iconClass = '';

				if ($form) {
					$form.on('form:reset', () => {
						$select.val(null)
							.trigger('change');
					});
				}

				if (icon) {
					iconClass = ` select2--icon-${icon}`;
				}

				const containerCssClass = `select2-selection--${size}${iconClass}`;
				const dropdownCssClass = `select2-dropdown--${size}`;

				const config = {
					width: $select.data('width') || '100%',
					theme: $select.data('select-theme') || 'white',
					containerCssClass,
					dropdownCssClass,
					placeholder,
					dropdownPosition: 'below',
					// dropdownParent,
					minimumResultsForSearch: search,
					closeOnSelect,
					language: {
						noResults: () => 'Совпадений не найдено',
					},
				};

				if (!isMultiple) {
					config.templateSelection = (data, container) => {
						if (data.element) {
							if (data.element.dataset.href) {
								$(container)
									.attr(
										'data-href',
										`${data.element.dataset.href}`,
									);
								return data.text;
							}
						}
						return data.text;
					};
				} else {
					config.templateSelection = (data) => {
						if (data.selected.length === 1) {
							return data.selected[0].text;
						}

						if (data.selected.length !== data.all.length) {
							if (texts) {
								if (data.selected.length < 5) {
									return `Выбрано ${data.selected.length} ${texts[0]}`; // типа, региона
								}
								return `Выбрано ${data.selected.length} ${texts[1]}`; // типов, регионов
							}
							return `Выбрано ${data.selected.length} из ${data.all.length}`;
						}
						if (texts) {
							return `Выбраны все ${texts[2]}`; // типы, регионы
						}
						return 'Выбрано все';
					};
					config.dropdownAdapter = DropdownAdapter;
					config.selectionAdapter = SelectionAdapter;
					config.resultsAdapter = ResultsAdapter;
					config.texts = texts;
				}

				$select.select2(config);

				$select.next()
					.find('.select2-selection')
					.addClass(containerCssClass);

				let ps;

				$select.on('select2:opening', () => {
					if (isMultiple) {
						setTimeout(() => {
							$('.select2-dropdown')
								.addClass('multiple')
								.addClass(dropdownCssClass);
						}, 0);
					}
				});
				// if (isMultiple) {
				// 	$select.on('select2:select', (e) => {
				// 		const { id } = e.params.data;
				// 		if (id === 'all') {
				// 			$select.find('option')
				// 				.prop('selected', 'selected');
				// 			$select.trigger('change.select2');
				// 			$select.select2('close');
				// 		}
				// 	});
				// }

				if (hasSearch) {
					$select.on('select2:open', () => {
						$('input.select2-search__field')
							.prop('placeholder', searchPlaceholder);
					});
				}

				$select.on('select2:open', (e) => {
					$('.select2-dropdown')
						.hide();
					setTimeout(() => {
						const content = document.querySelector(
							'.select2-results__options',
						);
						const modal = $select.closest('.modal');
						ps = new PerfectScrollbar(content, {
							suppressScrollX: true, // disable scrollX
						});
						$('.select2-dropdown')
							.slideDown(300, () => {
								ps && ps.update();
							});
						// remove first element highlight on open
						$('.select2-results__option')
							.removeClass('select2-results__option--highlighted');
					}, 0);

					const modal = document.querySelector('.mfp-wrap');
					if (modal) {
						modal.addEventListener('scroll', () => {
							$select.select2('close');
						});
					}
				});

				$select.on('select2:closing', function (e) {
					if (ps) {
						ps.destroy();
					}
				});

				$select.on('change', () => {
					if ($select.attr('multiple')) {
						const $placeholder = $select.attr(
							'data-select-placeholder',
						);
						const $field = $select.closest('.control')
							.find('input');
						setTimeout(() => {
							$field.attr('placeholder', $placeholder);
						}, 50);
					}
				});

				$select.on('select2:selecting', () => {
					setTimeout(() => {
						const result = $select
							.siblings('.select2-container')
							.find('.select2-selection__rendered')[0];

						if (result.dataset.href && result.dataset.href !== '#') {
							const $page = $('html, body');

							$page.animate(
								{
									scrollTop: $(result.dataset.href)
										.offset().top - $('.header')
										.outerHeight(),
								},
								400,
							);
						}
					}, 300);

					if ($select.attr('multiple')) {
						const $placeholder = $select.attr(
							'data-select-placeholder',
						);
						const $field = $select.closest('.control')
							.find('input');
						// setTimeout(() => {
						// 	$field.attr('placeholder', $placeholder);
						// }, 50);
					}
				});

				el.initialized = true;
			});
		});
	},
};

export default select;
