import $ from 'jquery';
import Swiper from 'swiper/js/swiper.min';
import lazyload from './lazyload';
import scrollAnimation from './sal';
import select from './select2/select';
import rellax from './rellax';
import counter from './counter';
import intlTelInput from './intl-tel-input';

// require('select2/dist/js/select2.full.js');

window.$ = window.jQuery = $;
window.Swiper = Swiper;

document.addEventListener('DOMContentLoaded', () => { // когда будет готов весь DOM
	scrollAnimation.init();
	select.init();
	rellax.init();
	intlTelInput.init();
});

require('./validation');

window.onload = () => { // когда загрузится весь контент (включая картинки, фреймы, объекты)
	counter.init();
};

window.addEventListener('init.validate', () => {
	validation.init();
});
window.addEventListener('init.lazyload', () => {
	lazyload.init();
});

window.addEventListener('init.cardAnimation', () => {
	scrollAnimation.initResultCardAnimate();
});

window.dispatchEvent(new CustomEvent('init.lazyload'));
