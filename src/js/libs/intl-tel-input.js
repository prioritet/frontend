__webpack_public_path__ = window.__webpack_public_path__ || '';
import { isTouch } from '../utils/breakpoints';
import mask from './mask';
import PerfectScrollbar from 'perfect-scrollbar';

export default {
	init() {
		mask.init();
		const elements = document.querySelectorAll('[data-mask-tel]');
		if (!elements.length) return;

		// Отложенная загрузка стилей
		const styles = import(/* webpackChunkName: "intl-tel-input" */ '../../scss/libs/intl-tel-input.scss');

		// Отложенная загрузка библиотеки
		const modul = import(/* webpackChunkName: "intl-tel-input" */ 'intl-tel-input');

		modul.then(({ default: intlTelInput }) => {
			window.intlTelInputGlobals.loadUtils('https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.0/js/utils.js');
			window.UPB.iti = window.UPB.iti || {};
			const initialCountry = window.UPB.iti.initial_country || 'ru';

			for (let i = 0; i < elements.length; i++) {
				const element = elements[i];
				const label = element.nextElementSibling;

				element.iti = intlTelInput(element, {
					autoPlaceholder: 'aggressive',
					initialCountry,
					autoHideDialCode: true,
					separateDialCode: true,
					preferredCountries: false, // Предпочитаемые страны,
					onlyCountries: ["az", "am", "by", "kz", "kg", "md", "ru", "tj", "uz", "ua"],
					utilsScript: '../../build/js/utils.js?1590403638580'
				});

				element.iti.telInput.addEventListener('countrychange', () => {
					clear();
					const countryInfo = element.iti.getSelectedCountryData();
					if (countryInfo.dialCode === '7') {
						element.iti.telInput.dispatchEvent(new CustomEvent('mask-init'));
					} else {
						element.iti.telInput.dispatchEvent(new CustomEvent('mask-destroy'));
					}
				});

				const iti = element.iti;
				const input = element.iti.telInput;
				const inputShell = input.closest('.input-shell');
				const errorMsg = inputShell.querySelector('.iti-error-msg');
				const lang = document.documentElement.getAttribute('lang');

				const errorMap = [['Invalid number', 'Неверный номер'],
					['Invalid country code', 'Неверный код страны'],
					['Too short', 'Слишком короткий номер'],
					['Too long', 'Слишком длинный номер'],
					['Invalid number', 'Неверный номер']];
				// var errorMap = ["Invalid number", "Invalid country code", "Too short", "Too long", "Invalid number"];
				const clear = function () {
					input.value = '';
				};
				const reset = function () {
					input.classList.remove('error');
					errorMsg.innerHTML = '';
					errorMsg.classList.add('hide');
				};

				input.addEventListener('open:countrydropdown', () => {
					setTimeout(() => {
						const list = element.closest('.iti').querySelector('.iti__country-list');
						if (list) {
							this.ps = new PerfectScrollbar(list, {
								suppressScrollX: true,
							})
						}
					}, 50);
				});

				input.addEventListener('close:countrydropdown', () => {
					if (this.ps) {
						this.ps.destroy();
					}
				});

				// on blur: validate
				input.addEventListener('blur', function () {
					reset();
					if (input.value.trim()) {
						if (!iti.isValidNumber()) {
							let secondIndex;
							lang === 'ru' ? secondIndex = 1 : secondIndex = 0;
							input.classList.remove('iti-success');
							input.classList.add('iti-error');
							const errorCode = iti.getValidationError();
							switch (errorCode) {
								case 0:
									errorMsg.innerHTML = errorMap[0][secondIndex];
									break;
								case 1:
									errorMsg.innerHTML = errorMap[1][secondIndex];
									break;
								case 2:
									errorMsg.innerHTML = errorMap[2][secondIndex];
									break;
								case 3:
									errorMsg.innerHTML = errorMap[3][secondIndex];
									break;
								default:
									errorMsg.innerHTML = errorMap[0][secondIndex];
							}
							errorMsg.classList.remove('hide');
						} else {
							input.classList.remove('iti-error');
							input.classList.add('iti-success');
						}
					}
				});

				// on keyup / change flag: reset
				input.addEventListener('change', reset);
				input.addEventListener('keyup', reset);


				// Список стран превращается в мобиле в попап. При открытии отключаем скроллы

				if (label) {
					/*
					*   Перемещаем label после инициализации
					*   Необходимая структура:
					*   iti
					*       iti-flag
					*       input
					*       .label
					*/

					element.iti.telInput.parentNode.appendChild(label);
				}
			}
		});
	}
};
