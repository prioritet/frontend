import Sal from 'sal.js';

export function isAnimated(el, className = 'animate') {
	return el.classList.contains(className);
}

export function animate(entry, className = 'animate') {
	entry.target.classList.add(className);
}

export function reverse(entry, className = 'animate') {
	entry.target.classList.remove(className);
}

const scrollAnimation = {
	threshold: 0.3,
	init() {
		const animation = new Sal({
			threshold: this.threshold,
			once: true,
			selector: '[data-counter]',
			animateClassName: 'animate',
			disabledClassName: 'animate-disabled',
			rootMargin: '50px',
		});

		// если js по каким-то причинам не загрузится, нужно чтобы все элементы были видны
		// поэтому изначально у тега html стоит класс animate-disabled, которые отключает анимацию появления при скролле
		// и только при подключении js этот класс убирается и все работает как надо
		document.documentElement.classList.remove('animate-disabled');
		window.addEventListener('init.scrollAnimation', () => {
			animation.enable();
		});
		this.initViewportAnimations();
		this.initAnimateUserText();
		this.initResultCardAnimate();
		this.initCardDirectionAnimate();
		this.initVacancyInfo();
	},
	initViewportAnimations() {
		const viewport = new Sal({
			threshold: this.threshold,
			once: true,
			selector: '[data-viewport-trigger]',
			animateClassName: 'in-viewport',
			disabledClassName: 'animate-disabled',
			rootMargin: '50px',
		});

		window.addEventListener('init.scrollAnimation', () => {
			viewport.enable();
			// viewportRepeat.enable();
		});
	},
	initSectionAnimations() {
		const sections = document.querySelectorAll('[data-section]');
		const inViewportClassName = 'in-viewport';

		const sectionsObserver = new IntersectionObserver((entries, observer) => {
			entries.forEach((entry) => {
				const repeat = entry.target.dataset.sectionRepeat !== undefined;
				if (entry.intersectionRatio >= 0.05) {
					animate(entry, inViewportClassName);
					if (!repeat) {
						observer.unobserve(entry.target);
					}
				} else if (repeat) {
					reverse(entry, inViewportClassName);
				}
			});
		}, {
			rootMargin: '50px',
			threshold: 0.05,
		});
	},
	// TODO: добавить анимацию появления текста при скролле внутри user-text
	initResultCardAnimate() {
		const cards = document.querySelectorAll('.filter-result-card');
		const cardsObserver = new IntersectionObserver((entries, observer) => {
			entries.forEach((entry) => {
				if (entry.intersectionRatio >= this.threshold) {
					animate(entry);
					observer.unobserve(entry.target);
				}
			});
		}, {
			rootMargin: '50px',
			threshold: this.threshold,
		});
		cards.forEach(card => {
			cardsObserver.observe(card);
		});
	},
	initCardDirectionAnimate() {
		const cards = document.querySelectorAll('.slider-directions__card');
		const cardsObserver = new IntersectionObserver((entries, observer) => {
			entries.forEach((entry) => {
				if (entry.intersectionRatio >= this.threshold) {
					animate(entry);
					observer.unobserve(entry.target);
				}
			});
		}, {
			rootMargin: '50px',
			threshold: this.threshold,
		});
		cards.forEach(card => {
			cardsObserver.observe(card);
		});
	},
	initVacancyInfo() {
		const infos = document.querySelectorAll('.vacancy-info__el');
		const infoObserver = new IntersectionObserver((entries, observer) => {
			entries.forEach((entry) => {
				if (entry.intersectionRatio >= this.threshold) {
					animate(entry);
					observer.unobserve(entry.target);
				}
			});
		}, {
			rootMargin: '50px',
			threshold: this.threshold,
		});
		infos.forEach(info => {
			infoObserver.observe(info);
		});
	},
	initAnimateUserText() {
		const userTexts = document.querySelectorAll('[data-animate-user-text]');

		const userTextOBserver = new IntersectionObserver((entries, observer) => {
			entries.forEach((entry) => {
				const repeat = entry.target.getAttribute('data-repeat') !== null;

				if (entry.intersectionRatio >= this.threshold) {
					animate(entry);

					if (!repeat) {
						observer.unobserve(entry.target);
					}
				} else if (repeat) {
					reverse(entry);
				}
			});
		}, {
			rootMargin: '50px',
			threshold: this.threshold,
		});

		userTexts.forEach((block) => {
			const toAnimate = block.children;
			const repeat = block.getAttribute('data-animate-user-text') === 'repeat';

			[].slice.call(toAnimate)
				.filter(el => el.tagName !== 'DIV')
				.forEach(el => {
					if(el.tagName === 'IMG') {
						el = wrapImg(block, el);
					}

					if (repeat) {
						el.setAttribute('data-repeat', '');
					}

					userTextOBserver.observe(el);
				});
		});

		function wrapImg(parentWrapper, el) {
			let div = document.createElement('div');
			let imgWrap = document.createElement('div');
			let elSibling = el.nextElementSibling;

			div.classList.add('user-text__img');
			imgWrap.classList.add('user-text__img-wrap');

			// Image wrapper for clip path
			imgWrap.appendChild(el);

			div.appendChild(imgWrap);

			parentWrapper.insertBefore(div, elSibling);

			return div;
		}
	},
};

export default scrollAnimation;
