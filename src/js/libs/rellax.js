import Rellax from 'rellax';

const rellax = {
	init() {
		const parallax = document.querySelectorAll('[data-parallax]');
		if (parallax.length) {
			// eslint-disable-next-line no-new
			new Rellax('[data-parallax]', {
				speed: -4,
				center: false,
				wrapper: null,
				round: true,
				vertical: true,
				horizontal: false,
			});
		}
	},
};

export default rellax;
