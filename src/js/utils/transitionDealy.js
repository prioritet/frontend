const transitionDelayDefinition = {
	init() {
		this.blocks = document.querySelectorAll('[data-delay]');
		if (this.blocks) {
			this.blocks.forEach(el => {
				const children = el.children;
				for (let i = 0, child; child = children[i]; i++) {
					if (!child.classList.contains('hidden')) {
						child.setAttribute('data-delay-el', '');
					}
				}
			});
		}
	},
	definition(block) {
		const definition = (el) => {
			const children = el.children;
			const delayMultiply = +(el.dataset.delay) / 10000 || 0.3;
			let realIndex = 0;
			for (let i = 0, child; child = children[i]; i++) {
				const childDisplay = getComputedStyle(child).display;
				if (!child.classList.contains('hidden') && childDisplay !== 'none') {
					const delay = delayMultiply + 0.13 * realIndex;
					child.style.transitionDelay = `${delay}s`;
					realIndex++;
				}
			}
		};
		if (block) {
			definition(block);
		} else if (this.blocks.length) {
			this.blocks.forEach(el => {
				definition(el);
			});
		}
	},

	destroy(block) {
		const destroy = (el) => {
			const children = el.children;
			for (let i = 0, child; child = children[i]; i++) {
				if (!child.classList.contains('hidden')) {
					child.style.transitionDelay = '';
				}
			}
		};
		if (block) {
			destroy(block);
		} else if (this.blocks.length) {
			this.blocks.forEach(el => {
				destroy(el);
			});
		}
	},
};

export default transitionDelayDefinition;
