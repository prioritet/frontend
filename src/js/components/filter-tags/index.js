(() => {
	let tagsWrap = document.querySelectorAll('.filter-tags');

	if(tagsWrap.length === 0) return;

	tagsWrap.forEach(el => {
		el.addEventListener('click', e => {
			let target = e.target.closest('[data-tag]');

			if(target){
				target.classList.toggle('active');
			}
		});
	});
})();
