import 'magnific-popup/dist/jquery.magnific-popup.min';
import $ from 'jquery';

document.addEventListener('DOMContentLoaded', () => {
	let link = document.querySelector('[data-expand-filter]');
	let fieldsWrap = document.querySelector('.filter-search-ext__fields-wrap');

	if (!link || !fieldsWrap) return;

	const linkText = link.querySelector('.link-action__text');
	const defaultText = linkText.textContent;
	const hideText = link.dataset.expandFilter;

	function filterToggle() {
		if (!fieldsWrap.classList.contains('active')) {
			linkText.textContent = hideText;
			fieldsWrap.classList.add('active');
		} else {
			fieldsWrap.classList.remove('active');
			linkText.textContent = defaultText;
		}
	}

	function filterClose() {
		fieldsWrap.classList.remove('active');
		linkText.textContent = defaultText;
	}

	function initPopup() {
		const $link = $(this);

		$link
			.magnificPopup({
				type: 'inline',
				midClick: false,
				mainClass: 'mfp-fade mfp-filter-popup',
				showCloseBtn: false,
				fixedContentPos: true,
				disableOn: function () {
					if ($(window).width() < 990) {
						return true;
					}
					return false;
				},
				callbacks: {
					close: function () {
						linkText.textContent = defaultText;
					},
				},
			});

		// Open
		$link
			.click(function () {
				$(this)
					.magnificPopup('open');
				linkText.textContent = hideText;
			});

		//Close
		$('.filter-popup__close')
			.click(function () {
				$.magnificPopup.close();
				linkText.textContent = defaultText;
			});
	}

	window.addEventListener('resize', _throttle(() => {
		if (window.innerWidth >= 990) {
			$.magnificPopup.close();
			link.addEventListener('click', filterToggle);
			link.removeEventListener('click', initPopup.bind(link));
		} else {
			filterClose();
			link.removeEventListener('click', filterToggle);
			link.addEventListener('click', initPopup.bind(link));
		}
	}, 300), {
		passive: true,
	});

	if (window.innerWidth >= 990) {
		link.addEventListener('click', filterToggle);
	} else {
		initPopup.bind(link)();
	}
});
