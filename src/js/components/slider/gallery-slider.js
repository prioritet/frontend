import Slider from './constructor';

const gallerySlider = new Slider({
	init: true,
	wrap: '[data-gallery-slider-wrap]',
	slider: '[data-gallery-slider]',
	prev: '[data-nav-arrow-prev]',
	next: '[data-nav-arrow-next]',
	options: {
		slidesPerView: 1,
		spaceBetween: 2,
		allowTouchMove: true,
		speed: 700,
		a11y: false,
		autoHeight: true,
		lazy: {
			loadPrevNext: true,
			elementClass: 'swiper-lazy',
		},
	},
});
