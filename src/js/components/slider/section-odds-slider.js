import Slider from './constructor';

const sectionOddsSlider = new Slider({
    init: true,
    wrap: '[data-section-odds-slider-wrap]',
    slider: '[data-section-odds-slider]',
    prev: '[data-nav-arrow-prev]',
    next: '[data-nav-arrow-next]',
    options: {
        slidesPerView: 1,
		slidesPerColumn: 1,
		slidesPerColumnFill: 'column',
        spaceBetween: 0,
        allowTouchMove: true,
		a11y: false,
        speed: 700,
        roundLengths: true,
        observeParents: true,
        observer: true,
        breakpoints: {
            [window.breakpoints.md]: {
                allowTouchMove: true,
				slidesPerView: 1,
				slidesPerColumn: 2,
			},
            [window.breakpoints.lg]: {
                allowTouchMove: false,
				slidesPerView: 2,
				slidesPerColumn: 1,
			},
        },
    },
});
