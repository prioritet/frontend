import { isDesktop, isTablet } from 'Utils/breakpoints';
import Slider from './constructor';

const navigationHide = (slides, slider) => {
	const wrap = slider.closest('[data-slider-cards-layout-wrap]');
	if ((slides.length < 4 && isDesktop()) || (slides.length < 3 && isTablet())) {
		if (!wrap) return;
		wrap.classList.add('nav-hidden');
	} else {
		if (wrap) {
			wrap.classList.remove('nav-hidden');
		}
	}
};

const dataSliderCardsLayout = new Slider({
	init: true,
	wrap: '[data-slider-cards-layout-wrap]',
	slider: '[data-slider-cards-layout]',
	prev: '[data-nav-arrow-prev]',
	next: '[data-nav-arrow-next]',
	options: {
		slidesPerView: 1,
		spaceBetween: 16,
		allowTouchMove: true,
		a11y: false,
		speed: 700,
		roundLengths: true,
		observeParents: true,
		observer: true,
		watchSlidesVisibility: true,
		breakpoints: {
			[window.breakpoints.md]: {
				spaceBetween: 2,
				allowTouchMove: true,
				slidesPerView: 2,
			},
			[window.breakpoints.lg]: {
				spaceBetween: 2,
				allowTouchMove: false,
				slidesPerView: 3,
			},
		},
		on: {
			init() {
				const slides = this.el.querySelectorAll('.swiper-slide');
				navigationHide(slides, this.el);
				window.addEventListener('resize', window._throttle(() => {
					navigationHide(slides, this.el);
				}, 300), {
					passive: true,
				});
			},
		},
	},
});
