import Slider from './constructor';
import { animate } from '../../libs/sal';

const sliderDirections = new Slider({
	init: true,
	wrap: '[data-slider-directions-wrap]',
	slider: '[data-slider-directions]',
	prev: '[data-nav-arrow-prev]',
	next: '[data-nav-arrow-next]',
	options: {
		slidesPerView: 1,
		slidesPerColumn: 1,
		slidesPerColumnFill: 'column',
		simulateTouch: false,
		a11y: false,
		speed: 700,
		roundLengths: true,
		observeParents: true,
		observer: true,
		spaceBetween: 16,
		breakpoints: {
			[window.breakpoints.md]: {
				slidesPerView: 2,
				slidesPerColumn: 2,
				spaceBetween: 2,
			},
			[window.breakpoints.lg]: {
				slidesPerView: 3,
				slidesPerColumn: 2,
				spaceBetween: 2,
			},
		},
	},
});
