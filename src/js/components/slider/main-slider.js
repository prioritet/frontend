import Slider from './constructor';

const mainSlider = new Slider({
	init: true,
	wrap: '[data-main-slider-wrap]',
	slider: '[data-main-slider]',
	prev: '[data-nav-arrow-prev]',
	next: '[data-nav-arrow-next]',
	options: {
		slidesPerView: 1,
		loop: true,
		effect: 'fade',
		speed: 300,
		a11y: false,
		lazy: {
			loadPrevNext: true,
			elementClass: 'swiper-lazy',
		},
		autoplay: {
			delay: 8000,
		},
		pagination: {
			el: '.hero-main__slider-pagination',
			type: 'bullets',
			clickable: true,
			bulletElement: 'button',
			bulletClass: 'hero-main__slider-bullet',
			bulletActiveClass: 'is-active',
		},
	},
});
