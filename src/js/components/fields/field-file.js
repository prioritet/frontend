window.addEventListener('init.upload', () => {
    const InputFile = function (element) {
        this.element = element;
        this.input = this.element.getElementsByClassName('file-upload__input')[0];
        this.label = this.element.getElementsByClassName('file-upload__label')[0];
        this.multipleUpload = this.input.hasAttribute('multiple'); // allow for multiple files selection

        // this is the label text element -> when user selects a file, it will be changed from the default value to the name of the file
        this.labelText = this.element.getElementsByClassName('file-upload__text')[0];
        this.labelSize = this.element.getElementsByClassName('file-upload__size')[0];
        this.button = this.element.getElementsByClassName('file-upload__label-icon-remove')[0];
        this.initialLabel = this.labelText.textContent;

        initInputFileEvents(this);
    };

    function initInputFileEvents(inputFile) {
        // make label focusable
        inputFile.label.setAttribute('tabindex', '0');
        inputFile.input.setAttribute('tabindex', '-1');

        // move focus from input to label -> this is triggered when a file is selected or the file picker modal is closed
        inputFile.input.addEventListener('focusin', function (event) {
            inputFile.label.focus();
        });

        // press 'Enter' key on label element -> trigger file selection
        inputFile.label.addEventListener('keydown', function (event) {
            if (event.keyCode && event.keyCode == 13 || event.key && event.key.toLowerCase() == 'enter') {
                inputFile.input.click();
            }
        });

        // file has been selected -> update label text
        inputFile.input.addEventListener('change', function (event) {
            inputFile.element.classList.add('uploaded');
            updateInputLabelText(inputFile);
        });

        inputFile.button.addEventListener('click', function (event) {
            event.preventDefault();
            inputFile.element.classList.remove('uploaded');
            inputFile.labelText.textContent = inputFile.initialLabel;
            inputFile.labelSize.textContent = '';
            inputFile.input.value = '';
        });
    }

    function updateInputLabelText(inputFile) {
        let label = '';
        let size = '';
        if (inputFile.input.files && inputFile.input.files.length < 1) {
            label = inputFile.initialLabel; // no selection -> revert to initial label
        } else if (inputFile.multipleUpload && inputFile.input.files && inputFile.input.files.length > 1) {
            label = inputFile.input.files.length + ' files'; // multiple selection -> show number of files
        } else {
            label = inputFile.input.value.split('\\').pop(); // single file selection -> show name of the file
			size = (inputFile.input.files[0].size / 1024).toFixed(2);
		}
        inputFile.labelText.textContent = label;
        inputFile.labelSize.textContent = `${size} kb`;
    }

    //initialize the InputFile objects
    const inputFiles = document.getElementsByClassName('file-upload');
    if (inputFiles.length > 0) {
        for (let i = 0; i < inputFiles.length; i++) {
            (function (i) {
                if (inputFiles[i].hasAttribute('data-upload-inited')) return;
                new InputFile(inputFiles[i]);
                inputFiles[i].setAttribute('data-upload-inited', true);
            })(i);
        }
    }
});

window.dispatchEvent(new CustomEvent('init.upload'));
