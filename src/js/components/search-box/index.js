import PerfectScrollbar from 'perfect-scrollbar';
import { isDesktop } from '../../../js/utils/breakpoints';

const searchBox = {
	init() {
		this.searchBox = document.querySelector('[data-search-box]');

		this.searchBoxInit(this.searchBox);
		// this.escInit();
	},

	searchBoxInit(searchInput) {
		if (searchInput) {
			const $result = $('[data-search-box-results]');
			const $resultHtml = $result.html();
			const $resultContent = $result.text().trim();
			const url = searchInput.dataset.searchBox;
			const $searchInput = $(searchInput);
			$searchInput.on('click', () => {
				if ($resultContent.length) {
					$result.fadeIn();
					const $list = $('[data-search-box-result-list]');
					if ($list.length) {
						const ps = new PerfectScrollbar('[data-search-box-result-list]', {
							wheelPropagation: false,
							suppressScrollX: true,
							minScrollbarLength: 32,
						});
					}
					const $items = $('[data-search-box-result-item]');
					if ($items) {
						$items.each((index, item) => {
							const $item = $(item);
							$item.on('click', (e) => {
								window.dispatchEvent(new CustomEvent('search-box-item:selected'));
								e.preventDefault();
								const $text = $item.text()
									.trim();
								$searchInput.val('');
								$searchInput.val($text);
							});
						});
					}
				}
			});
			$searchInput.on('keyup', function () {
				const search = $(this)
					.val();
				if ((search !== '') && (search.length > 1)) {
					$.ajax({
						type: 'GET',
						url: url,
						data: { 'search': search },
						success: function (msg) {
							$result.html(msg);
							const $resultContent = $result.text().trim();
							const $list = $('[data-search-box-result-list]');
							if ($list.length) {
								const ps = new PerfectScrollbar('[data-search-box-result-list]', {
									wheelPropagation: false,
									suppressScrollX: true,
									minScrollbarLength: 32,
								});
							}
							if (msg !== '' && $resultContent.length) {
								const $items = $('[data-search-box-result-item]');
								if ($items) {
									$items.each((index, item) => {
										const $item = $(item);
										$item.on('click', (e) => {
											e.preventDefault();
											const $text = $item.text()
												.trim();
											$searchInput.val('');
											$searchInput.val($text);
										});
									});
								}
								$result.fadeIn();
							} else {
								$result.fadeOut(100);
							}
						},
					});
				} else {
					$result.fadeOut(100);
					$result.html($resultHtml);
				}
			});

			$(document)
				.on('click', function (e) {
					if (!$(e.target)
						.closest('[data-search-box]').length) {
						$result.fadeOut(100);
						$result.html($resultHtml);
					}
				});
		}
	},
};

export default searchBox;
