window.addEventListener('init.to-bottom-button', () => {

	const btn = document.querySelector('[data-scroll-down-btn]');
	let windowHeight = window.innerHeight;
	if (!btn || btn.classList.contains('init')) return;
	window.addEventListener('resize', window._throttle(() => {
		windowHeight = window.innerHeight;
	}, 300));

	btn.addEventListener('click', (e) => {
		e.preventDefault();
		window.scrollTo({
			top: windowHeight,
			behavior: 'smooth',
		});
	});
	btn.classList.add('init');
});

window.dispatchEvent(new CustomEvent('init.to-bottom-button'));
