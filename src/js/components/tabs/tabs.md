#Tabs

```
const tabs = new Tabs({
    init: true, 
    wrap: '[data-tabs]', // Обёртка для табов и переключаемых блоков 
    tab: '.tabs__tab', // Селектор для табов
    content: '.tabs__content', // Селектор для переключаемых блоков
    activeClass: 'active', // Класс, который присваевается табу(кнопке), активному блоку
    underline: '.tabs__underline', // Селектор для линии, которая располагается под активным табом (необязательно)
});
```

- табам(кнопкам) необходимо добавить data-атрибут 'data-tab' с номером блока, например: data-tab="1"
```html
<button type="button" class="tabs__tab" data-tab="1">текст</button>
```
- блокам необходимо добавить data-атрибут 'data-content' с номером блока, например: data-content="1"
```html
<div class="block" data-content="1"></div>
```
- обёртка для табов c атрибутом data-tabs
```html
<div data-tabs>
    <div class="tabs">
        <button type="button" class="tabs__tab active" data-tab="1">1</button>
        <button type="button" class="tabs__tab" data-tab="2">2</button>
        <button type="button" class="tabs__tab" data-tab="3">3</button>
    </div>
    <div class="tabs__content">
        <div class="tab__block active" data-content="1">1</div>
        <div class="tab__block" data-content="2">2</div>
        <div class="tab__block" data-content="3">3</div>
    </div>
</div>
```

```text
Если класс active не будет задан, то он автоматически будет присвоен первому табу и блоку
```
