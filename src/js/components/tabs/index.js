export default class Tabs {
	constructor(props) {
		this.init = props.init;
		this.wrap = props.wrap;
		this.tab = props.tab;
		this.content = props.content;
		this.activeClass = props.activeClass;
		this.underline = props.underline;

		if (this.init) {
			this.render();
		}
	}

	render() {
		const wrap = document.querySelectorAll(this.wrap);
		for (let i = 0; i < wrap.length; i++) {
			const tabs = wrap[i].querySelectorAll(this.tab);
			const content = wrap[i].querySelectorAll(this.content);
			if (!this.defaultBehavior(tabs)) { // Если ни у одного из табов не проставлен класс active
				this.removeActiveState(tabs);
				this.removeActiveState(content);
				tabs[0].classList.add(this.activeClass);
			}
			this.tabsHandler(wrap[i], tabs, content);
			if (this.underline) {
				this.underlineInit(wrap[i], wrap[i].querySelector(`${this.tab}.${this.activeClass}`));
			}
			if (wrap[i].querySelector('.tabs--wide')) {
				window.addEventListener('resize', this.throttle(() => {
					this.update(wrap[i]);
				}, 200));
			}
			this.contentUpdate(wrap[i]);
		}
	}

	tabsHandler(wrap, tabs, content) {
		for (let i = 0; i < tabs.length; i++) {
			tabs[i].addEventListener('click', () => {
				tabs[i].classList.add(this.activeClass);
				this.removeActiveState(tabs, i);
				this.removeActiveState(content);
				if (tabs[i].getAttribute('data-tab') !== 'empty') {
					wrap.querySelector(`[data-content='${tabs[i].getAttribute('data-tab')}']`)
						.classList.add(this.activeClass);
				}
				if (this.underline) {
					this.underlineInit(wrap, tabs[i]);
				}
			});
		}
	}

	removeActiveState(arr, index = undefined) {
		for (let i = 0; i < arr.length; i++) {
			if (i === index) {
				continue;
			}
			arr[i].classList.remove(this.activeClass);
		}
	}

	defaultBehavior(tabs) {
		for (let i = 0; i < tabs.length; i++) {
			if (tabs[i].classList.contains('active')) return true;
		}
		return false;
	}

	underlineInit(wrap, tab) {
		const underlines = wrap.querySelectorAll(this.underline);
		const { offsetLeft } = tab;
		const width = tab.offsetWidth;
		for (let i = 0; i < underlines.length; i++) {
			underlines[i].style.transform = `translateX(${offsetLeft}px)`;
			underlines[i].style.width = `${width}px`;
		}
	}

	update(wrap) {
		const tab = wrap.querySelector(`${this.tab}.${this.activeClass}`);
		this.underlineInit(wrap, tab);
	}

	contentUpdate(wrap) {
		const target = wrap.querySelector(`${this.tab}.active`).getAttribute('data-tab').toString();
		if (target && target !== 'empty') {
			const content = wrap.querySelector(`[data-content='${target}']`);
			content.classList.add(this.activeClass);
		}
	}
}
