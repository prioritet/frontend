const searchBar = {
	init() {
		const topBarMenu = document.querySelector('.hero-top-bar');
		const heroSearch = document.querySelector('.hero-search');
		if (topBarMenu && !heroSearch) {
			this.topBarMenu = topBarMenu;
			this.searchButton = document.querySelector('[data-search-button]');
			this.searchBar = document.querySelector('.search-bar');

			this.TOGGLE_CLASS = 'hero-top-bar--search-open';

			this.searchButtonInit(this.searchButton);
			this.searchBarInit(this.searchBar);
			this.escInit();
		}
	},

	searchButtonInit(searchButton) {
		if (searchButton) {
			searchButton.addEventListener('click', (e) => {
				e.preventDefault();
				if (!this.topBarMenu.classList.contains(this.TOGGLE_CLASS)) {
					this.topBarMenu.classList.add(this.TOGGLE_CLASS);
					window.dispatchEvent(new CustomEvent('searchBarOpen'));
				} else if (this.topBarMenu.classList.contains(this.TOGGLE_CLASS)) {
					this.topBarMenu.classList.remove(this.TOGGLE_CLASS);
					window.dispatchEvent(new CustomEvent('searchBarClose'));
				}
			});
		}
	},

	searchBarInit(searchBar) {
		if (searchBar) {
			const formInput = searchBar.querySelector('.search-form__input');
			window.addEventListener('searchBarOpen', () => {
				if (formInput) {
					formInput.focus();
				}
			});
		}
	},

	escInit() {
		document.onkeydown = (e) => {
			e = e || window.event;
			let isEscape = false;
			if ('key' in e) {
				isEscape = (e.key === 'Escape' || e.key === 'Esc');
			} else {
				isEscape = (e.keyCode === 27);
			}
			if (isEscape && this.topBarMenu.classList.contains(this.TOGGLE_CLASS)) {
				this.topBarMenu.classList.remove(this.TOGGLE_CLASS);
				window.dispatchEvent(new CustomEvent('searchBarClose'));
			}
		};
	}
};

export default searchBar;
