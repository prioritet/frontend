# input
```
input: true,
type: "text",
name: "NAME",
label: "Введите имя",         // текст (label) перед инпутом
required: false,              // обязательный для заполнения
disabled: false,              // отключен
readonly: false,              // только для чтения
value: "Геннадий",            // дефолтное значение
autocomplete: "on"            // on | off
```
